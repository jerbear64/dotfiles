# Created by newuser for 5.3.1

setopt share_history
# setopt HIST_IGNORE_ALL_DUPS

if [ -f ~/.cache/wal/sequences ]; then
(cat ~/.cache/wal/sequences &)
fi

transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; } 

if grep -q "Fedora" /etc/*-release; then
alias dup="sudo dnf update --refresh"
alias din="sudo dnf install"
alias drm="sudo dnf remove"
alias dns="dnf search"
fi

if grep -q "Arch" /etc/*-release; then
alias dup="yay -Syu"
alias din="yay -S"
alias drm="yay -Rs"
alias dns="yay -Ss"
fi

if grep -q "Gentoo" /etc/*-release; then
#emerge() {sudo DARKELF_FEATURES="postmerge_distclean" emerge "$@"}
alias emerge="_ -E DARKELF_FEATURES=postmerge_distclean emerge"
fi

alias python=python3
alias pip=pip3
alias dkp="sudo pacman"
alias sfossdk=$PLATFORM_SDK_ROOT/sdks/sfossdk/mer-sdk-chroot

source $HOME/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle zsh-users/zsh-completions

antigen bundle svn

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search

antigen bundle "MichaelAquilina/zsh-you-should-use"

antigen theme agnoster

antigen apply

bindkey "^[[A" history-substring-search-up
bindkey "^[[B" history-substring-search-down



